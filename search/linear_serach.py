#linear search - given are the "a set (of numbers)", "n as parameter" and "key as the value we want to be found"
def linear_Search (list1, n, key):

#for dig in range(0,n) means that the range of the index we want to start to find is from 0 (index 0) up to n (up to -to a number)
    for dig in range(0,n):
        #simply, if the list1's digit is the same on key then return the digit that is same with the key
        if (list1[dig]==key):
            return dig
    #return means here if the number we want to find it not found inside the list it will end and prompt the condition we given which is return to -1   
    return -1

#list of digits, where to find, array start always from 0
list1 = [1,2,7,3,25,12]

#what to find in the list of digits
key = 25


n = len(list1)

#output of serach---set---var---find (what num to find)
res = linear_Search(list1, n, key)

#python if else
if(res==-1):
    #if the digit was not found...
    print("Not found")
else:
    #if the digit was found and where is the index was found
    print("Found at ", res)


def linear_search(list2, n, key2):
    for item in range(0,1):
        if(list2[item]==key2):
            return item
    return -1

#index starts counting in zero (0)
list2 = ["apple", "banana", "cashew", "durian"]
key2 = "cashew"

n = len(list2)
res = linear_Search(list2, n, key2)
if(res==-1):
    print("not found")
else:
    print("found at ", res)