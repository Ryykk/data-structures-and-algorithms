attempts = 1
position = -1


def binary_search(list, n):
    l = 0
    u = len(list) - 1

    while l <= u:
        mid = (l + u) // 2

        if list[mid] == n:
            globals()["position"] = mid
            return True
        else:
            if list[mid] < n:
                l = mid + 1
            else:
                u = mid - 1

    return False


values = input("Enter any numbers that are comma-separated (no spaces):\n")
n = int(input("Please enter the number you want to search for: \n"))
list = values.split(",")

for i in range(0, len(list)):
    list[i] = int(list[i])

if binary_search(list, n):
    print("Found! Index #:", position)
else:
    print("not found")
